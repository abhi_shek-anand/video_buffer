import 'dart:async';

import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:video_buffer/constants.dart';
import 'package:video_player/video_player.dart';

class VideoPlayer extends StatefulWidget {
  VideoPlayer({Key key}) : super(key: key);

  @override
  _VideoPlayerState createState() => _VideoPlayerState();
}

class _VideoPlayerState extends State<VideoPlayer> {
  FlickManager flickManager;
  List qualities = videoQualityMap.keys.toList();
  String videoUrl = "";
  String dropDownValue = "MilJul - 1080p - 167 MB";
  Size size;
  Timer _bufferingTimer;
  Duration timerInterval = Duration(
    seconds: 1,
  );
  int counter = 0;
  VideoPlayerController videoPlayerController;

  void startTimer() {
    _bufferingTimer = Timer.periodic(timerInterval, (_) {
      setState(() {
        counter++;
      });
    });
  }

  void resetTimer() {
    setState(() {
      counter = 0;
    });
    startTimer();
  }

  @override
  void initState() {
    videoUrl = videoQualityMap[dropDownValue];
    super.initState();
    videoPlayerController = VideoPlayerController.network(
      videoUrl,
    );
    flickManager = FlickManager(
      videoPlayerController: videoPlayerController,
    );
    if (videoPlayerController.value.initialized &&
        videoPlayerController.value.isBuffering) {
      startTimer();
    }
  }

  @override
  void dispose() {
    flickManager.dispose();
    super.dispose();
    _bufferingTimer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Video Buffer Test"),
      ),
      body: Center(
          child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: 10,
            ),
            child: Text("Buffering Time :- $counter seconds"),
          ),
          SizedBox(
            height: size.height * 0.1,
          ),
          Container(
            width: size.width * 0.9,
            height: size.height * 0.3,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            child: FlickVideoPlayer(
              flickManager: flickManager,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: DropdownButton(
              value: dropDownValue,
              items: List.generate(
                qualities.length,
                (index) => DropdownMenuItem(
                  value: qualities[index],
                  child: Text(
                    qualities[index],
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
              onChanged: (newValue) {
                resetTimer();
                setState(() {
                  dropDownValue = newValue;
                  flickManager.handleChangeVideo(
                    VideoPlayerController.network(
                      videoQualityMap[newValue],
                    ),
                  );
                });
              },
              elevation: 9,
              icon: Icon(
                Icons.settings,
              ),
              iconSize: 25,
            ),
          )
        ],
      )),
    );
  }
}
