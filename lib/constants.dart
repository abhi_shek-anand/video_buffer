Map videoQualityMap = {
  "File example - Uploaded to MilJul s3": "https://miljul-primary.s3.amazonaws.com/2020-11-18T10%3A22%3A36.189Zfile_example_MP4_1920_18MG.mp4",
  "File Examples - 1080P - 18 MB": "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_1920_18MG.mp4",
  "Self Upload - 25.79 MB": "https://miljul-primary.s3.amazonaws.com/2020-11-18T09%3A19%3A58.573ZVID_20201107_202151.mp4",
  "MilJul - 1080p - 167 MB":
      "https://miljul-primary.s3.amazonaws.com/collabvideos/2020-11-11T07_56_31_368_Birthday.MP4",
  "Other - 720p - 30 MB":
      "https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_30mb.mp4",
  "Other - 480p - 20 MB":
      "https://sample-videos.com/video123/mp4/480/big_buck_bunny_480p_20mb.mp4",
  "Other - 360p - 10 MB":
      "https://sample-videos.com/video123/mp4/360/big_buck_bunny_360p_10mb.mp4",
  "Other - 240p - 5 MB":
      "https://sample-videos.com/video123/mp4/240/big_buck_bunny_240p_5mb.mp4",
};
